console.log("Welcome to jQuery.");

$(document).ready(function(){

  $("#new-article").click(function(){

    // alert( $("#form-title").val() );
    // alert( $("#form-description").val() );

    var titulo = $("#form-title").val();
    var description = $("#form-description").val();
    var imagen = $("#form-image").val();

    var plantilla = "";
    plantilla += "<article>";
    plantilla += "<h2>";
    plantilla += titulo;
    plantilla += "</h2>";
    plantilla += "<p>";
    plantilla += description;
    plantilla += "</p>";
    plantilla += "<p>";
    plantilla += "<img src='"+imagen+"'></img>";
    plantilla += "</p>";
    plantilla += "</article>";

    $("#content").append(plantilla);

    // Resetear los valores del formulario
    $("#form-title").val("");
    $("#form-description").val("");
    $("#form-image").val("");


    // Para evitar que la página se recargue;
    return false;

  });


});
